#! /usr/bin/env python3

import argparse

def hello(name = 'world'):
    print('hello ' + name)

parser = argparse.ArgumentParser()
 
# Adding optional argument
parser.add_argument("-n", "--name", help = "name to greet")

arguments = parser.parse_args()

if arguments.name:
    hello(arguments.name)
    exit()

hello()
