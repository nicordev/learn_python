import my_exceptions

def greet(name = 'World'):
    if (isinstance(name, str) == False):
        raise my_exceptions.WrongParameterType('Parameter name must be a string')

    return f'Hello {name}!'
