import unittest
import greeter
import my_exceptions

class TestCalc(unittest.TestCase):

    def test_greet(self):
        '''Test function name must begin with test_'''
        result = greeter.greet()
        self.assertEqual(result, 'Hello World!')

    def test_greet_with_parameter(self):
        self.assertEqual(greeter.greet('Sarah'), 'Hello Sarah!')

    def test_greet_with_wrong_parameter_type(self):
        exception_has_been_thrown = False
        try:
            greeter.greet(1)
        except my_exceptions.WrongParameterType:
            exception_has_been_thrown = True

        self.assertTrue(exception_has_been_thrown, 'WrongParameterType exception not thrown')

'''
To avoid specifying the module in the command.

With it:
python3 test_greet.py

Without it:
python3 -m unittest test_greet.py
'''
if __name__ == '__main__':
    unittest.main()
