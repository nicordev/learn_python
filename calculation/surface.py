#! /usr/bin/env python3

def averageSurface(minimumWidth, maximumWidth, length):
    return ((minimumWidth + maximumWidth) / 2) * length

def planksSurface():
    return 2.22 * 15

print(averageSurface(296.5 / 100, 303 / 100, 532.5 / 100))
print(planksSurface())
